Algorithm:-

1.We first build a vocabulary dictionary using all the unique letters of the names in the corpus as keys and the index of each letter starting from zero (since Python is a zero-indexed language) in an ascending order. For our example, the vocabulary dictionary would be: {“a”: 0, “d”: 1, “i”: 2, “m”: 3}. Therefore, “imad” would become a list of the following integers: [2, 3, 0, 1].

2.Convert the input and the output characters to lists of integers using the vocabulary dictionary. In this post, we’ll assume that x¹ = \vec{0} for all examples. Therefore, y = “imad”and x = \vec{0}\+ “ima”. In other words, x^{t + 1} = y^twhich gives us: y = [2, 3, 0, 1]and x = [\vec{0}, 2, 3, 0].

3.For eachcharacter in the input:

4.Convert the input characters into one-hot vectors. Notice how the first character is: zero vector

5.Compute the hidden state layer.

6.Compute the output layer and then pass it through softmax to get the results as probabilities.

7.Feed the target character at time step (t) as the input character at time step (t + 1).

8.Go back to step A and repeat until we finish all the letters in the name.Limitations:-The model was build with 10,000 lines of corpus. So that the words which weare predicting doesn’t make any sence.
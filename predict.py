from numpy import array
from pickle import dump
import keras
from keras.utils import to_categorical
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from pickle import load
from keras.models import load_model
from keras.utils import to_categorical
from keras.preprocessing.sequence import pad_sequences
import torch
from pytorch_transformers import BertTokenizer, BertModel, BertForMaskedLM
# load doc into memory
def load_doc(filename):
    # open the file as read only
    file = open(filename, 'r')
    # read all text
    text = file.read()
    # close the file
    file.close()
    return text


# save tokens to file, one dialog per line
def save_doc(lines, filename):
    data = '\n'.join(lines)
    file = open(filename, 'w')
    file.write(data)
    file.close()
# load
pretrained_model = 'bert-base-multilingual-cased'
tokenizer = BertTokenizer.from_pretrained(pretrained_model)
print(tokenizer)
# Load pre-trained model (weights)
model = BertForMaskedLM.from_pretrained(pretrained_model)
model.eval()


# raw_text = load_doc('corpus.txt')
# # clean
# tokens = raw_text.split()
raw_text = str(tokenizer)
length = 10
sequences = list()
for i in range(length, len(raw_text)):
	# select sequence of tokens
	seq = raw_text[i-length:i+1]
	# store
	sequences.append(seq)
print('Total Sequences: %d' % len(sequences))
out_filename = 'char_sequences.txt'
save_doc(sequences, out_filename)


raw_text = load_doc(out_filename)
lines = raw_text.split('\n')

# integer encode sequences of characters
chars = sorted(list(set(raw_text)))
mapping = dict((c, i) for i, c in enumerate(chars))
sequences = list()
for line in lines:
    # integer encode line
    encoded_seq = [mapping[char] for char in line]
    # store
    sequences.append(encoded_seq)
print(mapping)
print(sequences)
# vocabulary size
vocab_size = len(mapping)
print('Vocabulary Size: %d' % vocab_size)

# separate into input and output
sequences = array(sequences)
X, y = sequences[:, :-1], sequences[:, -1]
sequences = [to_categorical(x, num_classes=vocab_size) for x in X]
X = array(sequences)
y = to_categorical(y, num_classes=vocab_size)

# define model
model = Sequential()
model.add(LSTM(75, input_shape=(X.shape[1], X.shape[2])))
model.add(Dense(vocab_size, activation='softmax'))
print(model.summary())
# compile model
model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
# fit model
model.fit(X, y, epochs=100, verbose=2)

# save the model to file
model.save('model.h5')
# save the mapping
dump(mapping, open('mapping.pkl', 'wb'))
# load doc into memory


"""
{'u': 34, 'S': 12, "'": 2, 's': 32, '\n': 0, 'T': 13, 'B': 7, 'H': 11, 'C': 8, 'o': 28, 'A': 6, 'e': 19, ',': 3, 'c': 17, 'b': 16, 'g': 21, 'a': 15, 'h': 22, 'p': 29, 'n': 27, 'm': 26, 'E': 9, 'i': 23, 'w': 35, 'F': 10, ';': 5, ' ': 1, '.': 4, 't': 33, 'r': 31, 'x': 36, 'y': 37, 'f': 20, 'W': 14, 'k': 24, 'd': 18, 'l': 25, 'q': 30}

"""
# import pickle
# with open('mapping.pkl', 'rb') as f:
#     data = pickle.load(f)
# print(data)

# model = keras.models.load_model( 'model.h5' )
# input = model.input
# output = model.output
# print(input)
# print(output)